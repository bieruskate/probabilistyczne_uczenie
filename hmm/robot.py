import pandas as pd
import numpy as np
from tqdm import tqdm

from hmm import HMM


# https://www.cs.princeton.edu/courses/archive/fall06/cos402/hw/hw5/hw5.html


def update_model_params_from_data(hmm, samples, viterbi_paths):
    hmm.emission_p = hmm._get_new_emission_p(samples, viterbi_paths)
    hmm.transition_p = hmm._get_new_transition_p(viterbi_paths)
    hmm.initial_p = hmm._get_new_initial_p(viterbi_paths)
    if hmm.use_log:
        hmm._convert_to_log()


if __name__ == '__main__':
    df = pd.read_csv('data/robot_no_momemtum.data', sep=' ')

    splits = df.index[df.position == '.'].tolist()
    splits.insert(0, -1)

    walks_obs = [df[s1 + 1:s2].color.values for s1, s2 in zip(splits, splits[1:])]
    walks_states = np.array([df[s1 + 1:s2].position.values for s1, s2 in zip(splits, splits[1:])])

    robot_hmm = HMM({}, {}, {})

    states = df.position.unique()
    states = states[states != '.']
    robot_hmm.states = states

    update_model_params_from_data(robot_hmm, walks_obs, walks_states)

    robot_hmm.visualize_graph('graphs/robot')

    print(robot_hmm.viterbi('rgbr'))

    train_split = 0.2
    split_n = int(len(walks_obs) * train_split)

    train_obs, test_obs = walks_obs[:split_n], walks_obs[split_n:]
    train_states, test_states = walks_states[:split_n], walks_states[split_n:]

    update_model_params_from_data(robot_hmm, train_obs, train_states)

    test_viterbi_paths = []
    for seq in tqdm(test_obs):
        test_viterbi_paths.append(robot_hmm.viterbi(seq)[0])

    accuracy = np.sum(test_states == test_viterbi_paths) / (test_states.shape[0] * test_states.shape[1])

    print(f'Accuracy with train split {train_split}: ', accuracy)
