from pprint import pprint
import numpy as np
from scipy.stats import bernoulli

from hmm import HMM

if __name__ == '__main__':
    noise_level = 0.1

    initial_p = {'F': .5, 'B': .5}

    transition_p = {
        'F': {'F': .5, 'B': .5},
        'B': {'B': .5, 'F': .5}
    }

    emission_p = {
        'F': {'H': .5, 'T': .9},
        'B': {'H': .9, 'T': .1}
    }

    samples = ['HHTHTH', 'THTHTH', 'TTHTTH', 'TTTHTH',
               'THHTTT', 'HHTHHT', 'HHTTHT', 'HTTTHH']

    hmm = HMM(initial_p, transition_p, emission_p)
    hmm.train(sigma=0.5, samples=samples)

    hmm.visualize_graph('graphs/learned-hmm')

    viterbi_paths = np.array([hmm.viterbi(seq)[0] for seq in samples])
    pprint(viterbi_paths)

    flip_dict = {'H': 'T', 'T': 'H'}
    samples_with_noise = []

    for sample in samples:
        s = list(sample)

        for i, letter in enumerate(sample):
            if bernoulli.rvs(p=noise_level):
                s[i] = flip_dict[letter]

        samples_with_noise.append(''.join(s))

    viterbi_paths_after_noise = np.array([hmm.viterbi(seq)[0] for seq in samples_with_noise])

    accuracy = np.sum(viterbi_paths == viterbi_paths_after_noise) / (viterbi_paths.shape[0] * viterbi_paths.shape[1])

    print('Accuracy with noise', noise_level, ':', accuracy)
