from hmm import HMM

if __name__ == '__main__':
    start_p = {'Healthy': 0.6, 'Fever': 0.4}

    trans_p = {
        'Healthy': {'Healthy': 0.7, 'Fever': 0.3},
        'Fever': {'Healthy': 0.4, 'Fever': 0.6}
    }

    emit_p = {
        'Healthy': {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
        'Fever': {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
    }

    hmm = HMM(initial_p=start_p, transition_p=trans_p, emission_p=emit_p)
    hmm.visualize_graph('graphs/graph')

    obs = ('normal', 'cold', 'dizzy')
    print(hmm.viterbi(obs))
