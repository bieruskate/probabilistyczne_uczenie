import subprocess

import numpy as np
import networkx as nx
from networkx.drawing.nx_agraph import write_dot


class HMM:
    def __init__(self, initial_p, transition_p, emission_p, use_log=False):
        self.use_log = use_log
        self._update_model_params(emission_p, initial_p, transition_p)

        self.states = self.initial_p.keys()

    def viterbi(self, observation_seq):
        mus = self._get_mu_k(len(observation_seq) - 1, [], observation_seq)

        best_finish = max(mus[-1].items(), key=lambda x: x[1]['p'])

        path = []
        previous_state = best_finish[0]

        i = len(mus) - 1
        while previous_state:
            path.append(previous_state)
            previous_state = mus[i][previous_state]['previous']
            i -= 1

        return list(reversed(path)), best_finish[1]['p']

    def train(self, sigma, samples, verbose=False):
        last_dist = np.inf
        distances_delta = last_dist

        if verbose:
            print('Distances delta:', distances_delta)

        while distances_delta > sigma:
            viterbi_paths = np.array([self.viterbi(seq)[0] for seq in samples])

            new_initial = self._get_new_initial_p(viterbi_paths)
            new_transitions = self._get_new_transition_p(viterbi_paths)
            new_emissions = self._get_new_emission_p(samples, viterbi_paths)

            total_dist = self._get_distance_to_current_params(new_emissions, new_initial, new_transitions)

            distances_delta = abs(last_dist - total_dist)
            last_dist = total_dist

            self._update_model_params(new_emissions, new_initial, new_transitions)

    def visualize_graph(self, filename='graph'):
        DG = nx.DiGraph()
        DG.add_node('Start', shape='doublecircle', color='green')
        DG.add_nodes_from(self.states, shape='Mcircle', color='blue')
        DG.add_nodes_from(self.emission_p[list(self.emission_p.keys())[0]].keys(), shape='septagon', color='orange')

        for state, p in self.initial_p.items():
            DG.add_edge('Start', state, label=np.round(p, 4))

        for state, targets in [*self.transition_p.items(), *self.emission_p.items()]:
            for target, p in targets.items():
                DG.add_edge(state, target, label=np.round(p, 4))

        write_dot(DG, 'graph.dot')
        subprocess.Popen(['dot', '-Tpdf', 'graph.dot', '-o', f'{filename}.pdf'])

    def _update_model_params(self, new_emissions, new_initial, new_transitions):
        self.initial_p = new_initial
        self.transition_p = new_transitions
        self.emission_p = new_emissions

        if self.use_log:
            self._convert_to_log()

    def _convert_to_log(self):
        self.initial_p = {k: np.log2(v) for k, v in self.initial_p.items()}
        self.transition_p = {k: {k2: np.log2(v2) for k2, v2 in v.items()} for k, v in self.transition_p.items()}
        self.emission_p = {k: {k2: np.log2(v2) for k2, v2 in v.items()} for k, v in self.emission_p.items()}

    def _get_distance_to_current_params(self, new_emissions, new_initial, new_transitions):
        init_dist = np.linalg.norm(np.array(list(self.initial_p.values())) - np.array(list(new_initial.values())))

        trans_dist = np.linalg.norm(np.array([list(row.values()) for row in self.transition_p.values()]) - \
                                    np.array([list(row.values()) for row in new_transitions.values()]))

        emission_dist = np.linalg.norm(np.array([list(row.values()) for row in self.emission_p.values()]) - \
                                       np.array([list(row.values()) for row in new_emissions.values()]))

        return init_dist + trans_dist + emission_dist

    def _get_new_emission_p(self, samples, viterbi_paths, laplace_corr=.01):
        em_values = np.unique(list(samples[0]))
        new_emissions = {state: {em: 0 for em in em_values} for state in self.states}

        total = {state: 0 for state in self.states}

        for path, seq in zip(viterbi_paths, samples):
            for p, s in zip(path, seq):
                new_emissions[p][s] += 1
                total[p] += 1

        for from_state in new_emissions:
            corrections = 0

            for to_state in new_emissions[from_state]:
                if new_emissions[from_state][to_state] == 0:
                    new_emissions[from_state][to_state] = laplace_corr
                    corrections += 1

            for to_state in new_emissions[from_state]:
                if new_emissions[from_state][to_state] != laplace_corr and corrections > 0:
                    new_emissions[from_state][to_state] -= laplace_corr / corrections

                new_emissions[from_state][to_state] /= total[from_state]

        return new_emissions

    def _get_new_transition_p(self, viterbi_paths):
        new_transitions = {state: {state: 0 for state in self.states} for state in self.states}

        for path in viterbi_paths:
            for i in range(len(path) - 1):
                new_transitions[path[i]][path[i + 1]] += 1

        for from_state in new_transitions:
            total = np.sum(list(new_transitions[from_state].values()))

            for to_state in new_transitions[from_state]:
                if total > 0:
                    new_transitions[from_state][to_state] /= total

        return new_transitions

    def _get_new_initial_p(self, viterbi_paths):
        paths_states, counts = np.unique(viterbi_paths[:, 0], return_counts=True)
        new_initial = {state: count / len(viterbi_paths) for state, count in zip(paths_states, counts)}

        for state in self.states:
            if state not in paths_states:
                new_initial[state] = 0

        return new_initial

    def _get_mu_k(self, k, mus, observation_seq):
        current_observation = observation_seq[k]

        operation = np.sum if self.use_log else np.product

        if k == 0:
            mus.append(
                {state: {'p': operation([self.initial_p[state], self.emission_p[state][current_observation]]),
                         'previous': None}
                 for state in self.states}
            )
        else:
            prev_mus = self._get_mu_k(k - 1, mus, observation_seq)[k - 1]
            state_probabilities = {}

            for state in self.states:
                max_prob = -np.inf
                best_prev = None

                for prev_state, prev_vals in prev_mus.items():
                    prob = operation([prev_vals['p'],
                                      self.transition_p[prev_state][state],
                                      self.emission_p[state][current_observation]])

                    if prob > max_prob:
                        max_prob = prob
                        best_prev = prev_state

                state_probabilities[state] = {'p': max_prob, 'previous': best_prev}

            mus.append(state_probabilities)

        return mus
